<?php

include 'EkskursiiExample.php';

$apiKey = '04a32e4f6b10027f6517908d03efd9ab'; // Получите свой ключ на  https://att.by/?agentstvam

$apiObj = new \EkskursiiExample($apiKey);
$objListResult = $apiObj->loadEkskursiiObjList(); // Метод для получения списка экскурсий
$abilityListResult = $apiObj->loadEkskursiiObjInfo([20016]); // Метод для получения списка экскурсий

$createOrder = $apiObj->makeOrder(
    20016,//id Экскурсии, полученный в п 2.1
    1,// - количество взрослых
    'TEST_FIO',// - ФИО туриста
    '2020-01-10 10:00:00',// - дата начала экскурсии в формате 'Y-m-d H:i:s'
    [
        'BLOCK_ID'         => 34606,// - id блока опционально
        'PARTNER_ORDER_ID' => 11111,// - id заявки в системе партнёра (если есть)
        'CHILD_COUNT'      => 0,// - количество детей
        'TOURIST_PHONE'    => '7777777',// - номер телефона туриста с кодом
        'TOURIST_COUNTRY'  => 'BY',// - Страна проживания туриста (двухбуквенный код Alpha2)
        'PRIM'             => 'Это тестовый заказ его нужно удалить',// - примечание к заказу
    ]
);
echo '
<html>
<title>Тестирование EKSKRUSII_API</title>
<body>
        <h1>API TEST</h1>
        <hr>
        <h2>Список экскурсий</h2>
        <div style="width:100%; height:35%; overflow: auto; background-color:#e8ffca;">
        <pre>'.print_r($objListResult, 1).'</pre>
        </div>
        <h2>Доступность экскурсий</h2>
        <div style="width:100%; height:35%; overflow: auto; background-color:#9bffca;">
        <pre>'.print_r($abilityListResult, 1).'</pre>
        </div>
        <div style="width:100%; height:35%; overflow: auto; background-color:#8bd9ff;">
        <pre>'.print_r($createOrder, 1).'</pre>
        </div>
 </body>
</html>';
flush();
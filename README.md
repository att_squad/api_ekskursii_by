# Разворачивание проекта при помощи Docker

Клонируйте репозиторий

``` bash
clone https://bitbucket.org/att_squad/api_ekskursii_by.git
```

Переключитесь в папку

``` bash
$ cd api_ekskursii_by
```

Выполните команду

``` bash
$ docker-compose up
```

Подождите установки.

``` bash
Building php
...
..
php_1  | [24-Oct-2019 20:12:27] NOTICE: fpm is running, pid 1
php_1  | [24-Oct-2019 20:12:27] NOTICE: ready to handle connections
```

Откройте в браузере http://localhost:8080. Для полноценной работы вам необходим API_KEY, который можно запросить по ссылке https://att.by/?agentstvam

# ***Описание работы класса API Ekskursii.by***

Информация об использовании API  <https://att.by/api/ekskursii/>

**php 7.1+**
##Класс предоставляет возможность использовать API Ekskursii.

####Для использования Вам необходимо:
1. Скачать класс EkskursiiExample.php
2. Скачать объект с передачей в конструктор вашего ключа API
3. Выполнить один из методов класса с передачей в него необходимых параметров
4. Результатом будет объект JSON с запрашиваемой информацией(или результатом выполнения запроса)

Например :
```php
<?php
$apiKey='XXX_YOUR_API_KEY_XXXX'; // в этой переменной должен быть ключ API 
$ApiObj= new \EkskursiiExample($apiKey);
$ExecuteResult= $ApiObj->loadEkskursiiObjList(); // Метод для получения списка экскурсий
?>
```

#### Доступны следующие методы:

* **loadEkskursiiObjList()** - получение списка экскурсий
* **loadEkskursiiObjInfo($obj_id)** - получение информации об экскурсии по ID $obj_id может быть числом либо массивом чисел
* **makeOrder($objId, $adultCount, $touristName, $orderStart, [array $addOrderParam])** - создание заказа, где

    * **$objId** - id экскурсии полученный из списка экскурсий
    * **$adultCount** - количество взрослых
    * **$touristName** - ФИО туриста
    * **$orderStart** - дата начала экскурсии в формате 'Y-m-d H:i:s'
    * **$addOrderParam** - необязательный параметр, позволяющий передать дополнительный параметры в виде ассоциативного массива, например
     ```php <?php
      array('PARTNER_ORDER_ID'=>13234,'TOURIST_PHONE'=>'+375291234567')
     ?>```

* **makeOrderGroup($orderData)** - создание заказа на сборную экскурсию с получением ссылки на оплату. $orderData - массив параметров
    ```php <?php
               array(
                      'OBJ_ID'           =>0,
                      'PARTNER_ORDER_ID '=>'',
                      'ADULT_COUNT'      =>0,
                      'CHILD_COUNT'      =>0,
                      'TOURIST_NAME'     =>'',
                      'TOURIST_EMAIL'    =>'',
                      'TOURIST_COUNTRY'  =>'',
                      'TOURIST_PHONE'    =>'',
                      'ORDER_START'      =>'',
                      'PRIM '            =>'',
                      'BLOCK_ID '        =>0, //Optional
                  )
         ?>```
    
#### Поддержка осуществляется:
  * по адресу it@att.by пометкой "Ekskursii API" 
  * в телеграм чате https://t.me/+dLjXSO4r3CU0NDEy
  * через контактного менеджера Экскурсионного отдела АТТ
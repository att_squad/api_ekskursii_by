<?php

/**
 * Class EkskursiiExample
 */
final class EkskursiiExample
{

    /**
     * @var string
     */
    public $apiKey = '';

    /**
     * EkskursiiExample constructor.
     *
     * @param  string  $apiKey
     */
    public function __construct($apiKey = '')
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Загрузка Списка экскурсий
     *
     * @return mixed
     */
    public function loadEkskursiiObjList()
    {
        $requestData = [
            'GET_DATA' => 'OBJ_LIST',
        ];

        return $this->getRequest($requestData);
    }

    /**
     * @param  array  $requestData
     *
     * @return mixed
     */
    private function getRequest($requestData = [])
    {
        $requestData += ['API_KEY' => $this->apiKey,];

        $context = stream_context_create([
            'http' =>
                [
                    'method' => 'POST',
                    'content' => http_build_query($requestData),
                ],
        ]);
        $response = file_get_contents('https://att.by/api/ekskursii/', false, $context);

        return json_decode($response);
    }

    /**
     * Загрузка данных об экскурсии
     *
     * @param $obj_id  - id экскурсии
     *
     * @return mixed
     */
    public function loadEkskursiiObjInfo($obj_id)
    {
        if (!is_array($obj_id)) {
            $obj_id = [$obj_id];
        }
        $requestData = [
            'GET_DATA' => 'OBJ_INFO',
            'OBJ_ID' => implode('|', $obj_id),
        ];

        return $this->getRequest($requestData);
    }

    /**
     * Создание нового заказа
     *
     * @param  integer  $objId          - id экскурсии
     * @param  integer  $adultCount     - количесиво взрослых
     * @param  string   $touristName    - ФИО туриста
     * @param  string   $orderStart     - дата начала экскурсии
     * @param  array    $addOrderParam  - массив необязательных параметров
     *
     * @return mixed
     */
    public function makeOrder($objId, $adultCount, $touristName, $orderStart, $addOrderParam = [])
    {
        $requestData = [
            'PUT_DATA' => 'NEW_ORDER',
            'ORDER_DATA' => [
                    'OBJ_ID' => $objId,
                    'ADULT_COUNT' => $adultCount,
                    'TOURIST_NAME' => $touristName,
                    'ORDER_START' => $orderStart,
                ] + (array)$addOrderParam,
        ];

        return $this->getRequest($requestData);
    }

    /**
     * Создание нового заказа - сборная экскурсия с получением ссылки на оплату
     *
     * @param  array  $orderData
     *
     * @return mixed
     */
    public function makeOrderGroup(
        array $orderData
        = [
            'OBJ_ID' => 0,
            'PARTNER_ORDER_ID ' => '',
            'ADULT_COUNT' => 0,
            'CHILD_COUNT' => 0,
            'TOURIST_NAME' => '',
            'TOURIST_EMAIL' => '',
            'TOURIST_COUNTRY' => '',
            'TOURIST_PHONE' => '',
            'ORDER_START' => '',
            'PRIM ' => '',
        ]
    ) {
        $requestData = [
            'PUT_DATA' => 'NEW_ORDER_GROUP',
            'ORDER_DATA' => $orderData,
        ];

        return $this->getRequest($requestData);
    }
}